package com.example.balado.bnspsoal2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ViewKontak extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    Button viewKembali,viewSimpan;
    EditText viewNomor,viewNama,viewTgl,viewjk,viewAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_kontak);
        dbHelper = new DataHelper(this);
        viewNomor = findViewById(R.id.viewNomor);
        viewNama = findViewById(R.id.viewNama);
        viewTgl = findViewById(R.id.viewTgl);
        viewjk = findViewById(R.id.viewjk);
        viewAlamat = findViewById(R.id.viewAlamat);
        viewSimpan = findViewById(R.id.viewSimpan);
        viewKembali = findViewById(R.id.viewKembali);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        cursor=db.rawQuery("SELECT noHp,nama,tgl,jk,alamat from kontak where nama = '"+
        getIntent().getStringExtra("nama")+"'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0){
            cursor.moveToPosition(0);
            viewNomor.setText(cursor.getString(0).toString());
            viewNama.setText(cursor.getString(1).toString());
            viewTgl.setText(cursor.getString(2).toString());
            viewjk.setText(cursor.getString(3).toString());
            viewAlamat.setText(cursor.getString(4).toString());
        }

        viewKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }
}
