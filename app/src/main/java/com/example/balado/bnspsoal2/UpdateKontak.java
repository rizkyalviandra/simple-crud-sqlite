package com.example.balado.bnspsoal2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateKontak extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    Button updateKembali,updateSimpan;
    EditText updateNomor,updateNama,updateTgl,updatejk,updateAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_kontak);

        dbHelper = new DataHelper(this);

        updateNomor = findViewById(R.id.updateNomor);
        updateNama = findViewById(R.id.updateNama);
        updateTgl = findViewById(R.id.updateTgl);
        updatejk = findViewById(R.id.updatejk);
        updateAlamat = findViewById(R.id.updateAlamat);

        updateSimpan = findViewById(R.id.updateSimpan);
        updateKembali = findViewById(R.id.updateKembali);

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        cursor=db.rawQuery("SELECT noHp,nama,tgl,jk,alamat from kontak where nama = '"+
                getIntent().getStringExtra("nama")+"'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0){
            cursor.moveToPosition(0);
            updateNomor.setText(cursor.getString(0).toString());
            updateNama.setText(cursor.getString(1).toString());
            updateTgl.setText(cursor.getString(2).toString());
            updatejk.setText(cursor.getString(3).toString());
            updateAlamat.setText(cursor.getString(4).toString());
        }

        updateSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update kontak set nama='"+
                        updateNama.getText().toString() +"', tgl='" +
                        updateTgl.getText().toString()+"', jk='"+
                        updatejk.getText().toString() +"', alamat='" +
                        updateAlamat.getText().toString() + "' where noHp='" +
                        updateNomor.getText().toString()+"'");

                Toast.makeText(getApplicationContext(),"Update Sukses!!",Toast.LENGTH_SHORT).show();
                MainActivity.ma.RefreshList();
                finish();
            }
        });

        updateKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }
}
