package com.example.balado.bnspsoal2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Tambah extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;
    Button btnSimpan, btnKembali;
    EditText txtNomor,txtNama,txtTgl,txtjk,txtAlamat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);

        dbHelper = new DataHelper(this);
        txtNomor = findViewById(R.id.txtNomor);
        txtNama = findViewById(R.id.txtNama);
        txtTgl = findViewById(R.id.txtTgl);
        txtjk = findViewById(R.id.txtjk);
        txtAlamat = findViewById(R.id.txtAlamat);
        btnSimpan = findViewById(R.id.btnSimpan);
        btnKembali = findViewById(R.id.btnKembali);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("insert into kontak(noHp,nama,tgl,jk,alamat) values('"+
                        txtNomor.getText().toString()+"','"+
                        txtNama.getText().toString()+"','"+
                        txtTgl.getText().toString()+"','"+
                        txtjk.getText().toString()+"','"+
                        txtAlamat.getText().toString()+"')");
                Toast.makeText(getApplicationContext(),"Insert Sukses!!", Toast.LENGTH_SHORT).show();
                MainActivity.ma.RefreshList();
                finish();
            }
        });

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }
}
